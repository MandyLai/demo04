package com.example.demo04.controller;

import com.example.demo04.annotation.LoginRequired;
import com.example.demo04.pojo.User;
import com.example.demo04.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
@Api(value = "Transaction list API.")
public class TransactionController extends BaseController {

    @Autowired
    private TransactionService transactionService;

    @LoginRequired
    @ApiOperation(value = "Get paginated transaction list.")
    @GetMapping("/")
    public ResponseEntity<?> getTransactions(@ApiParam(value = "pattern: yyyy", required = true) @RequestParam int year,
                                             @ApiParam(value = "the month-of-year, from 1 (January) to 12 (December).", required = true) @RequestParam int month,
                                             @ApiParam(value = "page number, starting at 0.") @RequestParam(required = false, defaultValue = "0") int page,
                                             @ApiParam(value = "page size, default is 10.") @RequestParam(required = false, defaultValue = "10") int pageSize) throws Exception {

        User user = new User("123456");
        return new ResponseEntity<>(transactionService.getTransactions(user.getUserId(), year, month, page, pageSize), HttpStatus.OK);
    }

}
