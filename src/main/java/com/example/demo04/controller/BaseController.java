package com.example.demo04.controller;

import com.example.demo04.pojo.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

public class BaseController {

    protected final User getUser() {
        return (User) Optional.ofNullable(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()))
                .map(ServletRequestAttributes::getRequest).map(it -> it.getAttribute("user")).orElse(null);
    }

}
