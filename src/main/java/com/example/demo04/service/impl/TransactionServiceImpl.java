package com.example.demo04.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.demo04.pojo.ExchangeRate;
import com.example.demo04.pojo.Transaction;
import com.example.demo04.repository.ExchangeRateRepository;
import com.example.demo04.responseentity.TransactionsResp;
import com.example.demo04.service.TransactionService;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private String default_currency = "USD";

    @Autowired
    private KafkaConsumer<String, String> kafkaConsumer;
    @Autowired
    private ExchangeRateRepository exchangeRateService;

    @Override
    public TransactionsResp getTransactions(String userId, int year, int month, int page, int pageSize) throws Exception {
        // Get transactions from kafka
        List<Transaction> transactions = getTransactionsFromKafka(userId, year, month);

        // Exchange rate conversion on transaction amount
        Map<String, Map<Date, CompletableFuture<ExchangeRate>>> currencyDateRateMap = getCurrencyDateRateMap(transactions);//, today);
        calculateAmountOnDefaultCurrency(transactions,currencyDateRateMap);

        transactions.sort(Comparator.comparing(Transaction::getDate).reversed());

        // Calculate total income and total expense in the month.
        Pair<BigDecimal, BigDecimal> totalIncomeAndExpense = calculateTotalIncomeAndExpense(transactions);

        // TODO if possible, it's better to cache the transactions data.

        // Get paginated list.
        List<Transaction> result = transactions.stream().skip((long) page * pageSize).limit(pageSize).collect(Collectors.toList());

        return new TransactionsResp(result, totalIncomeAndExpense.getLeft(), totalIncomeAndExpense.getRight());
    }

    private Pair<BigDecimal, BigDecimal> calculateTotalIncomeAndExpense(List<Transaction> transactions) {
        BigDecimal totalIncome = BigDecimal.ZERO;
        BigDecimal totalExpense = BigDecimal.ZERO;
        for (Transaction transaction : transactions) {
            if (transaction.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                totalIncome = totalIncome.add(transaction.getAmountDefaultCurrency());
            } else {
                totalExpense = totalExpense.add(transaction.getAmountDefaultCurrency().abs());
            }
        }
        return Pair.of(totalIncome, totalExpense);
    }

    private void calculateAmountOnDefaultCurrency(List<Transaction> transactions, Map<String, Map<Date, CompletableFuture<ExchangeRate>>> currencyDateRateMap) throws ExecutionException, InterruptedException {
        for (Transaction transaction : transactions) {
            String currency = transaction.getCurrency();
            Date date = Date.from(transaction.getDate().atZone(ZoneId.systemDefault()).toInstant());
            ExchangeRate exchangeRate = currencyDateRateMap.get(currency).get(date).get();
            BigDecimal amountDefaultCurrency = transaction.getAmount().multiply(exchangeRate.getRate());
            transaction.setAmountDefaultCurrency(amountDefaultCurrency);
        }
    }

    private Map<String, Map<Date, CompletableFuture<ExchangeRate>>> getCurrencyDateRateMap(List<Transaction> transactions){//}, Date today) {
        Map<String, Map<Date, CompletableFuture<ExchangeRate>>> currencyDateRateMap = new HashMap<>();
        for (Transaction transaction : transactions) {
            String currency = transaction.getCurrency();
            Date date = Date.from(transaction.getDate().atZone(ZoneId.systemDefault()).toInstant());
            Map<Date, CompletableFuture<ExchangeRate>> dateRateMap = currencyDateRateMap.computeIfAbsent(currency, v -> new HashMap<>());
            dateRateMap.computeIfAbsent(date, v -> exchangeRateService.getExchangeRate(currency, default_currency, date));
        }
        return currencyDateRateMap;
    }

    private List<Transaction> getTransactionsFromKafka(String userId, int year, int month) {
        // Construct the Kafka topic name based on the user ID and year/month
        String topicName = String.format("transactions_%s_%d_%02d", userId, year, month);
        // Subscribe to the Kafka topic
        kafkaConsumer.subscribe(Collections.singleton(topicName));
        List<Transaction> transactions = new ArrayList<>();
        kafkaConsumer.seekToBeginning(Collections.emptySet());
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofSeconds(5));
            if (records.isEmpty()) {
                break;
            }
            for (ConsumerRecord<String, String> record : records) {
                Transaction transaction = JSON.parseObject(record.value(), Transaction.class);
                if (transaction != null && transaction.getDate().getYear() == year && transaction.getDate().getMonthValue() == month) {
                    transactions.add(transaction);
                }
            }
        }
        return transactions;
    }
}
