package com.example.demo04.service;

import com.example.demo04.responseentity.TransactionsResp;

public interface TransactionService {

    TransactionsResp getTransactions(String userId, int year, int month, int page, int pageSize) throws Exception;

}
