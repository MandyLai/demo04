package com.example.demo04.interceptor;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Payload;
import com.example.demo04.annotation.LoginRequired;
import com.example.demo04.constants.HttpHeaders;
import com.example.demo04.exception.AuthorizationException;
import com.example.demo04.pojo.User;
import com.example.demo04.util.TokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LoginRequiredInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRequired loginRequired = method.getAnnotation(LoginRequired.class);
        if (loginRequired == null) {
            return true;
        }
        String token = request.getHeader(HttpHeaders.TOKEN);
        if (StringUtils.isBlank(token)) {
            throw new AuthorizationException("Token is required");
        }
        User user = getUser(token);
        request.setAttribute("user", user);
        return true;
    }

    private User getUser(String token) {
        DecodedJWT decodedJWT = TokenUtil.verifyToken(token);
        Map<String, Claim> claims = Optional.ofNullable(decodedJWT).map(Payload::getClaims).orElse(new HashMap<>());
        String uid = Optional.ofNullable(claims.get("uid")).map(Claim::asString).orElse(null);
        if (StringUtils.isBlank(uid)) {
            throw new AuthorizationException("Token is invalid.");
        }
        return new User(uid);
    }


}


