package com.example.demo04.interceptor;

import com.example.demo04.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Slf4j
public class LogInterceptor  implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        log.info("start {} {}", request.getMethod(), getUrl(request));
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return;
        }
        log.info("end {} {}, userId = {}, status = {}", request.getMethod(), getUrl(request), getUserId(request), response.getStatus());
    }

    private String getUserId(HttpServletRequest request) {
        return Optional.ofNullable((User) request.getAttribute("user")).map(User::getUserId).orElse("NONE");
    }

    private String getUrl(HttpServletRequest request) {
        String url = request.getRequestURI();
        if (StringUtils.isNotBlank(request.getQueryString())) {
            url += "?" + request.getQueryString();
        }
        return url;
    }

}
