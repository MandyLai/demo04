package com.example.demo04.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo04.exception.AuthorizationException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TokenUtil {

    private static final String JWT_SECRET = "This is a secret.";
    private static final Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET);

    private static JWTCreator.Builder createTokenBuilder() {
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        return JWT.create().withHeader(map)
                .withIssuedAt(now)
                .withExpiresAt(calendar.getTime());
    }

    public static String createToken(String userId) {
        JWTCreator.Builder builder = createTokenBuilder();
        return builder.withClaim("uid", userId).sign(algorithm);
    }

    public static DecodedJWT verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            //TODO add log
            e.printStackTrace();
            if (e instanceof TokenExpiredException) {
                throw new AuthorizationException("Token is expired.");
            }
            throw new AuthorizationException("Token is invalid.");
        }
        return jwt;
    }

    public static void main(String[] args) {
        // TODO generating token for test
        String token = createToken("123456");
        System.out.println(token);
        DecodedJWT decodedJWT = verifyToken(token);
        Map<String, Claim> claims = decodedJWT.getClaims();
        String uid = claims.get("uid").asString();
        System.out.println(uid);
    }


























}
