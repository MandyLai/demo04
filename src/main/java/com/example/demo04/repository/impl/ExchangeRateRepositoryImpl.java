package com.example.demo04.repository.impl;

import com.example.demo04.pojo.ExchangeRate;
import com.example.demo04.repository.ExchangeRateRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Service
public class ExchangeRateRepositoryImpl implements ExchangeRateRepository {

    @Override
    public CompletableFuture<ExchangeRate> getExchangeRate(String from, String to, Date date) {
        if (StringUtils.equals(from, to)) {
            return CompletableFuture.completedFuture(new ExchangeRate(from, to, date, BigDecimal.valueOf(1.0)));
        }
        // TODO Get exchange rate from the third-party provider.
        return CompletableFuture.completedFuture(new ExchangeRate(from, to, date, BigDecimal.valueOf(0.16)));
    }

}
