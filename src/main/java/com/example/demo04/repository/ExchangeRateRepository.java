package com.example.demo04.repository;

import com.example.demo04.pojo.ExchangeRate;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

public interface ExchangeRateRepository {

    CompletableFuture<ExchangeRate> getExchangeRate(String from, String to, Date date);

}
