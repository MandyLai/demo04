package com.example.demo04.exception;

import org.springframework.http.HttpStatus;

public class AuthorizationException extends CustomException {

    public AuthorizationException(String message) {
        super(message, null, HttpStatus.UNAUTHORIZED);
    }

}
