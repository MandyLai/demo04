package com.example.demo04.responseentity;

import com.example.demo04.pojo.Transaction;

import java.math.BigDecimal;
import java.util.List;

public class TransactionsResp {

    private List<Transaction> transactions;
    private BigDecimal totalIncome;
    private BigDecimal totalExpense;

    public TransactionsResp() {
    }

    public TransactionsResp(List<Transaction> transactions, BigDecimal totalIncome, BigDecimal totalExpense) {
        this.transactions = transactions;
        this.totalIncome = totalIncome;
        this.totalExpense = totalExpense;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(BigDecimal totalExpense) {
        this.totalExpense = totalExpense;
    }
}
