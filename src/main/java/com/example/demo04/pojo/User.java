package com.example.demo04.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class User {

    private String userId;
    private String token;

    public User(String userId) {
        this.userId = userId;
    }

}
