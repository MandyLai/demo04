package com.example.demo04.pojo;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class Transaction {

    private String id;
    private String currency;
    private String account;
    private LocalDateTime date;
    private BigDecimal amount;
    private BigDecimal amountDefaultCurrency;

    public Transaction(String id, String currency, String account, LocalDateTime date, BigDecimal amount) {
        this.id = id;
        this.currency = currency;
        this.account = account;
        this.date = date;
        this.amount = amount;
    }

}
