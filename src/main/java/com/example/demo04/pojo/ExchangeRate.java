package com.example.demo04.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRate {

    private String from;
    private String to;
    private Date date;
    private BigDecimal rate;

}
