package com.example.demo04;

import mockit.integration.junit4.JMockit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

@RunWith(JMockit.class)
public class Demo04ApplicationTests {

	@Mock
	ConfigurableApplicationContext ctx;

	@Test
	public void testMain() throws Exception {
		new mockit.MockUp<SpringApplication>() {
			@mockit.Mock
			public ConfigurableApplicationContext run(Class<?> primarySource, String... args) {
				return ctx;
			}
		};
		Demo04Application.main(new String[0]);
	}

}
