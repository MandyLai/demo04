package com.example.demo04.controller;

import com.example.demo04.pojo.Transaction;
import com.example.demo04.pojo.User;
import com.example.demo04.responseentity.TransactionsResp;
import com.example.demo04.service.TransactionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private TransactionController transactionController;

    @BeforeEach
    void mockUpBaseController() {
        User user = new User("userId");

        new mockit.MockUp<BaseController>() {
            @mockit.Mock
            User getUser() {
                return user;
            }
        };
    }

    @Test
    public void testGetTransactions() throws Exception {

        mockUpBaseController();

        int year = 2022;
        int month = 1;
        int page = 0;
        int pageSize = 10;
        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction = new Transaction("01", "USD", "abc", LocalDateTime.of(2022, 1, 1, 0, 0, 0), new BigDecimal("-1000"));
        transaction.setAmountDefaultCurrency(transaction.getAmount());
        Transaction transaction2 = new Transaction("02", "CNY", "abc", LocalDateTime.of(2022, 1,2, 0, 0, 0), new BigDecimal("1500"));
        transaction2.setAmountDefaultCurrency(transaction2.getAmount().multiply(BigDecimal.valueOf(0.16)));
        Transaction transaction3 = new Transaction("03", "USD", "abc", LocalDateTime.of(2022, 1, 3, 0, 0, 0), new BigDecimal("-1200"));
        transaction3.setAmountDefaultCurrency(transaction3.getAmount());
        transactions.add(transaction);
        transactions.add(transaction2);
        transactions.add(transaction3);

        when(transactionService.getTransactions(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(new TransactionsResp(transactions, BigDecimal.valueOf(240), BigDecimal.valueOf(2200)));

        ResponseEntity<?> result = transactionController.getTransactions(year, month, page, pageSize);
        TransactionsResp resp = (TransactionsResp) result.getBody();
        Assertions.assertEquals(3, resp.getTransactions().size());
        Assertions.assertEquals(0, resp.getTotalIncome().compareTo(BigDecimal.valueOf(240)));
        Assertions.assertEquals(0, resp.getTotalExpense().compareTo(BigDecimal.valueOf(2200)));
    }

}
