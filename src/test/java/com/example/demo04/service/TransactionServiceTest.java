package com.example.demo04.service;

import com.alibaba.fastjson.JSON;
import com.example.demo04.pojo.ExchangeRate;
import com.example.demo04.pojo.Transaction;
import com.example.demo04.repository.ExchangeRateRepository;
import com.example.demo04.service.impl.TransactionServiceImpl;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @Mock
    private KafkaConsumer<String, String> kafkaConsumer;

    @Mock
    private ExchangeRateRepository exchangeRateService;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Test
    public void testCalculateTotalIncomeAndExpense() throws Exception {
        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction = new Transaction("01", "USD", "abc", LocalDateTime.of(2022, 1, 1, 0, 0, 0), new BigDecimal("-1000"));
        transaction.setAmountDefaultCurrency(transaction.getAmount());
        Transaction transaction2 = new Transaction("02", "CNY", "abc", LocalDateTime.of(2022, 1,2, 0, 0, 0), new BigDecimal("1500"));
        transaction2.setAmountDefaultCurrency(transaction2.getAmount().multiply(BigDecimal.valueOf(0.16)));
        Transaction transaction3 = new Transaction("03", "USD", "abc", LocalDateTime.of(2022, 1, 3, 0, 0, 0), new BigDecimal("-1200"));
        transaction3.setAmountDefaultCurrency(transaction3.getAmount());
        transactions.add(transaction);
        transactions.add(transaction2);
        transactions.add(transaction3);

        Method method = transactionService.getClass().getDeclaredMethod("calculateTotalIncomeAndExpense", List.class);
        method.setAccessible(true);
        Pair<BigDecimal, BigDecimal> result = (Pair<BigDecimal, BigDecimal>)method.invoke(transactionService, transactions);
        Assertions.assertEquals(0, result.getLeft().compareTo(BigDecimal.valueOf(240.0)));
        Assertions.assertEquals(0, result.getRight().compareTo(BigDecimal.valueOf(2200.0)));
    }

    @Test
    public void testCalculateAmountOnDefaultCurrency() throws Exception {
        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction = new Transaction("01", "USD", "abc", LocalDateTime.of(2022, 1, 1, 0, 0, 0), new BigDecimal("-1000"));
        Transaction transaction2 = new Transaction("02", "CNY", "abc", LocalDateTime.of(2022, 1,2, 0, 0, 0), new BigDecimal("1500"));
        Transaction transaction3 = new Transaction("03", "USD", "abc", LocalDateTime.of(2022, 1, 3, 0, 0, 0), new BigDecimal("-1200"));
        transactions.add(transaction);
        transactions.add(transaction2);
        transactions.add(transaction3);

        Map<String, Map<Date, CompletableFuture<ExchangeRate>>> currencyDateRateMap = new HashMap<>();
        Map<Date, CompletableFuture<ExchangeRate>> dateRateMapForUSD = new HashMap<>();
        Date date = Date.from(transaction.getDate().atZone(ZoneId.systemDefault()).toInstant());
        dateRateMapForUSD.put(date, CompletableFuture.completedFuture(new ExchangeRate(transaction.getCurrency(), "USD", date, BigDecimal.valueOf(1.0))));
        Map<Date, CompletableFuture<ExchangeRate>> dateRateMapForCNY = new HashMap<>();
        Date date2 = Date.from(transaction2.getDate().atZone(ZoneId.systemDefault()).toInstant());
        dateRateMapForCNY.put(date2, CompletableFuture.completedFuture(new ExchangeRate(transaction2.getCurrency(), "USD", date2, BigDecimal.valueOf(0.16))));
        Date date3 = Date.from(transaction3.getDate().atZone(ZoneId.systemDefault()).toInstant());
        dateRateMapForUSD.put(date3, CompletableFuture.completedFuture(new ExchangeRate(transaction3.getCurrency(), "USD", date3, BigDecimal.valueOf(1.0))));
        currencyDateRateMap.put("USD", dateRateMapForUSD);
        currencyDateRateMap.put("CNY", dateRateMapForCNY);

        Method method = transactionService.getClass().getDeclaredMethod("calculateAmountOnDefaultCurrency", List.class, Map.class);
        method.setAccessible(true);
        method.invoke(transactionService, transactions, currencyDateRateMap);
        Assertions.assertEquals(transaction.getAmount().multiply(dateRateMapForUSD.get(date).get().getRate()), transaction.getAmountDefaultCurrency());
        Assertions.assertEquals(transaction2.getAmount().multiply(dateRateMapForCNY.get(date2).get().getRate()), transaction2.getAmountDefaultCurrency());
        Assertions.assertEquals(transaction3.getAmount().multiply(dateRateMapForUSD.get(date3).get().getRate()), transaction3.getAmountDefaultCurrency());
    }

    @Test
    public void testGetCurrencyDateRateMap() throws Exception {
        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction = new Transaction("01", "USD", "abc", LocalDateTime.of(2022, 1, 1, 0, 0, 0), new BigDecimal("-1000"));
        Transaction transaction2 = new Transaction("02", "CNY", "abc", LocalDateTime.of(2022, 1,2, 0, 0, 0), new BigDecimal("1500"));
        Transaction transaction3 = new Transaction("03", "USD", "abc", LocalDateTime.of(2022, 1, 3, 0, 0, 0), new BigDecimal("-1200"));
        transactions.add(transaction);
        transactions.add(transaction2);
        transactions.add(transaction3);

        CompletableFuture<ExchangeRate> rateFuture = CompletableFuture.completedFuture(new ExchangeRate("USD", "USD", Date.from(transaction.getDate().atZone(ZoneId.systemDefault()).toInstant()), BigDecimal.valueOf(1.0)));
        CompletableFuture<ExchangeRate> rateFuture2 = CompletableFuture.completedFuture(new ExchangeRate("CNY", "USD", Date.from(transaction2.getDate().atZone(ZoneId.systemDefault()).toInstant()), BigDecimal.valueOf(0.16)));
        CompletableFuture<ExchangeRate> rateFuture3 = CompletableFuture.completedFuture(new ExchangeRate("USD", "USD", Date.from(transaction3.getDate().atZone(ZoneId.systemDefault()).toInstant()), BigDecimal.valueOf(1.0)));
        Mockito.when(exchangeRateService.getExchangeRate(Mockito.anyString(), Mockito.anyString(), Mockito.any(Date.class)))
                .thenReturn(rateFuture)
                .thenReturn(rateFuture2)
                .thenReturn(rateFuture3);

        Method getTransactionsMethod = transactionService.getClass().getDeclaredMethod("getCurrencyDateRateMap", List.class);
        getTransactionsMethod.setAccessible(true);
        Map<String, Map<Date, CompletableFuture<ExchangeRate>>> result = (Map<String, Map<Date, CompletableFuture<ExchangeRate>>>) getTransactionsMethod.invoke(transactionService, transactions);
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(BigDecimal.valueOf(1.0), result.get(transaction.getCurrency()).get(Date.from(transaction.getDate().atZone(ZoneId.systemDefault()).toInstant())).get().getRate());
        Assertions.assertEquals(BigDecimal.valueOf(0.16), result.get(transaction2.getCurrency()).get(Date.from(transaction2.getDate().atZone(ZoneId.systemDefault()).toInstant())).get().getRate());
        Assertions.assertEquals(BigDecimal.valueOf(1.0), result.get(transaction3.getCurrency()).get(Date.from(transaction3.getDate().atZone(ZoneId.systemDefault()).toInstant())).get().getRate());
    }

    @Test
    public void testGetTransactionsFromKafka() throws Exception {

        List<ConsumerRecord<String, String>> consumerRecords = new ArrayList<>();
        ConsumerRecord<String, String> consumerRecord = new ConsumerRecord<>("topic", 0, 0, "01", JSON.toJSONString(new Transaction("01", "USD", "abc", LocalDateTime.of(2022, 1, 1, 0, 0, 0), new BigDecimal("-1000"))));
        ConsumerRecord<String, String> consumerRecord2 = new ConsumerRecord<>("topic", 0, 10, "02", JSON.toJSONString(new Transaction("02", "CNY", "abc", LocalDateTime.of(2022, 1,2, 0, 0, 0), new BigDecimal("1500"))));
        ConsumerRecord<String, String> consumerRecord3 = new ConsumerRecord<>("topic", 0, 20, "03", JSON.toJSONString(new Transaction("03", "USD", "abc", LocalDateTime.of(2022, 1, 3, 0, 0, 0), new BigDecimal("-1200"))));
        ConsumerRecord<String, String> consumerRecord4 = new ConsumerRecord<>("topic", 0, 30, "04", JSON.toJSONString(new Transaction("04", "USD", "abc", LocalDateTime.of(2022, 2, 3, 0, 0, 0), new BigDecimal("-1000"))));
        ConsumerRecord<String, String> consumerRecord5 = new ConsumerRecord<>("topic", 0, 40, "05", JSON.toJSONString(new Transaction("05", "CNY", "abc", LocalDateTime.of(2023, 1, 3, 0, 0, 0), new BigDecimal("1800"))));
        consumerRecords.add(consumerRecord);
        consumerRecords.add(consumerRecord2);
        consumerRecords.add(consumerRecord3);
        consumerRecords.add(consumerRecord4);
        consumerRecords.add(consumerRecord5);
        TopicPartition topicPartition = new TopicPartition("transactions", 0);

        Mockito.when(kafkaConsumer.poll(Mockito.any(Duration.class)))
                .thenReturn(new ConsumerRecords<>(Collections.singletonMap(topicPartition, consumerRecords)))
                .thenReturn(new ConsumerRecords<>(new HashMap<>()));

        String userId = "";
        int year = 2022;
        int month = 1;
        Method getTransactionsMethod = transactionService.getClass().getDeclaredMethod("getTransactionsFromKafka", String.class, int.class, int.class);
        getTransactionsMethod.setAccessible(true);
        List<Transaction> transactions = (List<Transaction>) getTransactionsMethod.invoke(transactionService, userId, year, month);
        Assertions.assertEquals(3, transactions.size());
        Assertions.assertEquals(year, transactions.get(0).getDate().getYear());
        Assertions.assertEquals(year, transactions.get(1).getDate().getYear());
        Assertions.assertEquals(year, transactions.get(2).getDate().getYear());
        Assertions.assertEquals(month, transactions.get(0).getDate().getMonthValue());
        Assertions.assertEquals(month, transactions.get(1).getDate().getMonthValue());
        Assertions.assertEquals(month, transactions.get(2).getDate().getMonthValue());
    }

}
