# demo04

There is a REST API for returning the paginated list of money account transactions.

Before retrieving the data, it is RECOMMENDED to store the transaction list in kafka with the topic "transactions_userId_year_month"(e.g. transactions_123456_2022_01).
When requesting the interface, the code will retrieve all the transaction list from the kafka topic named "transactions_userId_year_month" then do the exchange rate conversion, and calculate the total expenditure and total income for the month.

## Getting started

### requirements

- java-1.8
- maven
- kafka
- docker

### Prepare

- Run `docker pull zookeeper` and `docker pull wurstmeister/kafka` to pull images.
- Run `docker-compose up` in folder `demo04` to deploy zookeeper and kafka.
- Send the transaction list in kafka with the topic "transactions_userId_year_month"(e.g. transactions_123456_2022_01). The message just like `{"account":"123456_account_USD","amount":20,"currency":"USD","date":"2022-01-01T00:00:30","id":"1"}`;
- Modify the Kafka-related configuration in application.yml if necessary.

### Deploy

#### Mode 1
- Run the main method in Demo04Application.java. After deployment, the /transactions API could successfully obtain the transaction list in kafka.

#### Mode 2
- Run `mvn clean package` to generate the jar package.
- Run `docker build ...` to build a Docker image.
- Run `docker run ... ` to create a container and deploy the application.
- Or update the image in k8s.yml and run `kubectl apply -f k8s.yml` to deploy the application according to the configuration.

Such as:
```
mvn clean package
docker build --build-arg JAR_FILE=target/demo04-0.0.1-SNAPSHOT.jar -f Dockerfile -t demo04:v1.0.2 .
docker run -p 8090:8090 --name demo04container2 demo04:v1.0.2
```

Note: After deployment, the /transactions API fail to obtain the transaction list in kafka. The issue has not yet been resolved.

## Swagger

http://localhost:8090/swagger-ui/index.html

## Business process

![image](IMG/Process.png)



##### TODO
- Integrate the exchange rate api from the third-party provider.
- There are some issues when consume kafka data after deploying demo04 and kafka in docker.











